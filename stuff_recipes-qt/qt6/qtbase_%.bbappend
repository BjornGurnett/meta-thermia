inherit qt6-features
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

# From arago
#GLES_EXTRA_DEPS = "libdrm"
#PACKAGECONFIG[gles2] = "-opengl es2 -eglfs,,virtual/libgles2 virtual/egl ${GLES_EXTRA_DEPS}"

#PACKAGECONFIG[gles2] = "-opengl es2 -eglfs -qpa eglfs,,virtual/libgles2 virtual/egl libdrm"
#PACKAGECONFIG[opengl] = "" 
GLES_EXTRA_DEPS = "libdrm"

PACKAGECONFIG[gles2] = "-opengl es2 -eglfs,,virtual/libgles2 virtual/egl ${GLES_EXTRA_DEPS}"


# static  
PACKAGECONFIG:append = " release commercial kms gbm fontconfig gles2 eglfs sql-sqlite gif"
PACKAGECONFIG:remove = "  tests examples wayland qtwebkit qtsensors qtcharts qttools xcb gl"
EXTRA_OECONF:append = " -qpa eglfs"
QT_QPA_PLATFORM ??= "eglfs"

QT_CONFIG_FLAGS += "-qpa ${QT_QPA_PLATFORM}"

#do_configure_prepend() {
#cat > ${S}/mkspecs/oe-device-extra.pri <<EOF
#QMAKE_LIBS_EGL += -lEGL -ldl -lglib-2.0 -lpthread
#QMAKE_LIBS_OPENGL_ES2 += -lGLESv2 -lgsl -lEGL -ldl -lglib-2.0 -lpthread

#QMAKE_CFLAGS += -DLINUX=1 -DWL_EGL_PLATFORM
#QMAKE_CXXFLAGS += -DLINUX=1 -DWL_EGL_PLATFORM

#QT_QPA_DEFAULT_PLATFORM = eglfs
#EOF
#}



