USE_RUBY = ""
RDEPENDS_${PN}:remove = "qtcharts-dev"
RDEPENDS_${PN}:remove = "qtcharts-mkspecs"
RDEPENDS_${PN}:remove = "qtcharts-qmlplugins"

RDEPENDS_${PN}:remove = "qttools-dev"
RDEPENDS_${PN}:remove = "qttools-mkspecs"
RDEPENDS_${PN}:remove = "qttools-staticdev"


RDEPENDS_${PN}:remove = "qtsensors-dev"
RDEPENDS_${PN}:remove = "qtsensors-mkspecs"
RDEPENDS_${PN}:remove = "qtsensors-plugins"
RDEPENDS_${PN}:remove = "qtsensors-qmlplugins"

RDEPENDS_${PN}:remove = "qt3d-dev"
RDEPENDS_${PN}:remove = "qt3d-mkspecs"
RDEPENDS_${PN}:remove = "qt3d-qmlplugins"

RDEPENDS_${PN}:remove = "qtenginio-dev"
RDEPENDS_${PN}:remove = "qtenginio-mkspecs"
RDEPENDS_${PN}:remove = "qtenginio-qmlplugins"

RDEPENDS_${PN}:remove = "qtlocation-dev"
RDEPENDS_${PN}:remove = "qtlocation-mkspecs"
RDEPENDS_${PN}:remove = "qtlocation-plugins"
RDEPENDS_${PN}:remove = "qtlocation-qmlplugins"

RDEPENDS_${PN}:remove = "qtwebchannel-dev"
RDEPENDS_${PN}:remove = "qtwebchannel-mkspecs"
RDEPENDS_${PN}:remove = "qtwebchannel-qmlplugins"

