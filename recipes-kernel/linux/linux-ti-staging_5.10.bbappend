FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append=" file://am335x-thermia.dts;subdir=git/arch/arm/boot/dts"

#SRC_URI:append=" file://fragment_CONFIG_RTC_DRV_PCF8563.cfg"
#KERNEL_CONFIG_FRAGMENTS:append=" ${WORKDIR}/fragment_CONFIG_RTC_DRV_PCF8563.cfg"
#SRC_URI:append=" file://fragment_ETHERNET.cfg"
#KERNEL_CONFIG_FRAGMENTS:append=" ${WORKDIR}/fragment_ETHERNET.cfg"
#SRC_URI:append=" file://fragment_MISC.cfg"
#KERNEL_CONFIG_FRAGMENTS:append=" ${WORKDIR}/fragment_MISC.cfg"
#SRC_URI:append=" file://fragment_MUSB.cfg"
#KERNEL_CONFIG_FRAGMENTS:append=" ${WORKDIR}/fragment_MUSB.cfg"
#SRC_URI:append=" file://fragment_TOUCHSCREEN.cfg"
#KERNEL_CONFIG_FRAGMENTS:append=" ${WORKDIR}/fragment_TOUCHSCREEN.cfg"
SRC_URI:append=" file://fragment_SOUND.cfg"
KERNEL_CONFIG_FRAGMENTS:append=" ${WORKDIR}/fragment_SOUND.cfg"
#SRC_URI:append=" file://fragment_REGULATORS.cfg"
#KERNEL_CONFIG_FRAGMENTS:append=" ${WORKDIR}/fragment_REGULATORS.cfg"
#SRC_URI:append=" file://fragment_I2C.cfg"
#KERNEL_CONFIG_FRAGMENTS:append=" ${WORKDIR}/fragment_I2C.cfg"
SRC_URI:append=" file://fragment_GraphicsSupport.cfg"
KERNEL_CONFIG_FRAGMENTS:append=" ${WORKDIR}/fragment_GraphicsSupport.cfg"

SRC_URI:append=" file://fragment_BootScreen.cfg"
KERNEL_CONFIG_FRAGMENTS:append=" ${WORKDIR}/fragment_BootScreen.cfg"

SRC_URI:append=" file://fragment_UART.cfg"
KERNEL_CONFIG_FRAGMENTS:append=" ${WORKDIR}/fragment_UART.cfg"







