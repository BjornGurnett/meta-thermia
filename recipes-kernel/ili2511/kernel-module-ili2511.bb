SUMMARY = "ili2511 touchscreen"
SECTION = "FOO/apps"
LICENSE = "MIT"
LIC_FILES_CHKSUM = \
"file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"
PR="r0"
inherit module

SRC_URI = "file://ILI2511.c \
	   file://ILI2511.h \
	   file://Makefile \
"

S = "${WORKDIR}"



