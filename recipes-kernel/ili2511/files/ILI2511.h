#ifndef __LINUX_ILI2511_TS_H__
#define __LINUX_ILI2511_TS_H__

struct ILI2511ts_platform_data {
	unsigned int Gain;
	unsigned int Threshold;
	unsigned int Offset;
	unsigned int XSensors;
	unsigned int YSensors;
	unsigned int Firmware;
	unsigned int Size;
	unsigned int MaxX;
	unsigned int MaxY;
	unsigned int DeviceMode;
	unsigned int CurrentColumn;
	unsigned int TimeToMonitorMode;
};


/*
enum ILI2511_ts_regs {
	ILI2511_REG_DEVICE_MODE	= 0x00,
	ILI2511_REG_THRESHOLD	   = 0x80,
	ILI2511_REG_TIME_TO_MONITOR_MODE = 0x87,
	ILI2511_REG_GAIN	      = 0x92,
	ILI2511_REG_OFFSET,
	ILI2511_REG_X_SENSORS,
	ILI2511_REG_Y_SENSORS,
	ILI2511_REG_HIBERNATE    = 0xA5,
	ILI2511_REG_FIRMWARE_VERSION,
	ILI2511_REG_SIZE_INFO    = 0xA8,
}; */

// set up HW
   #define I2C_BUS_NUM 1
   #define I2C_ADR (0x41)
   #define EXT_TOUCH_IRQ 55
   #define EXT_TOUCH_RST 58
   #define MAX_TOUCH_POINTS 10

// SW info
#define VERSION "v0.1"
#define CTP_NAME "EDT ILI2511"

#endif



