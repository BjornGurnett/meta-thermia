#include <linux/module.h>
#include <linux/hrtimer.h>
#include <linux/slab.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/i2c.h>
#include <linux/irq.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/input.h>
#include <linux/input/mt.h>
#include <linux/gpio.h> 

#include "ILI2511.h"  //local definitions

struct ts_event {
	u8  touch_point;
	u16 X[MAX_TOUCH_POINTS];
	u16 Y[MAX_TOUCH_POINTS];
	u8  Status;
	u16 pressure;
};

struct release_work {
	u16 index;
	u8 ison;
	struct delayed_work 	dwork;
};

struct ILI2511_ts_data {
	struct input_dev	*input_dev;
	struct ts_event		event;
	struct work_struct 	pen_event_work;
	struct release_work 	dwork[MAX_TOUCH_POINTS];
	struct workqueue_struct *ts_workqueue;
};

static struct ILI2511ts_platform_data *pdata;
static struct i2c_client *this_client;

static int I2C_CTPWrite(u8* Data,int Size)
{
	struct i2c_msg msgs[] = {
		{
			.addr	= this_client->addr,
			.flags	= 0,
			.len	= Size,
			.buf	= Data,
		},
	};
	int Err = i2c_transfer(this_client->adapter, msgs, 1);
	if (Err < 0)
		pr_err("msg %s i2c write error: %d\n", __func__, Err);
	return Err;
}

static int I2C_CTPRead(u8* Data,int Size)
{
	struct i2c_msg msgs[] = {
		{
			.addr	= this_client->addr,
			.flags	= I2C_M_RD,
			.len	= Size,
			.buf	= Data,
		},
	};
	int Err = i2c_transfer(this_client->adapter, msgs, 1);
	if (Err < 0)
		pr_err("msg %s i2c read error: %d\n", __func__, Err);
	return Err;
}


static ssize_t ILI2511GetReset(struct device *dev, struct device_attribute *attr, char *buf);
static ssize_t ILI2511SetReset(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);

static ssize_t ILI2511DummyRead(struct device *dev, struct device_attribute *attr, char *buf);
static ssize_t ILI2511DummyWrite(struct device *dev, struct device_attribute *attr, const char *buf, size_t count);

struct ILI2511SysFunctionAttribute {
	struct device_attribute attr;
	int Value;
};

static struct ILI2511SysFunctionAttribute SySFsResetAttribute = { __ATTR(Reset, 0664, ILI2511GetReset, ILI2511SetReset), 0};
static struct ILI2511SysFunctionAttribute SySFsDummyAttribute = { __ATTR(Dummy, 0664, ILI2511DummyRead, ILI2511DummyWrite), 0};

static struct attribute *ILI2511ts_attributes[] = {
	&SySFsResetAttribute.attr.attr,
	&SySFsDummyAttribute.attr.attr,
	NULL
};

static const struct attribute_group ILI2511ts_attr_group = {
	.attrs = ILI2511ts_attributes
};

static ssize_t ILI2511GetReset(struct device *dev, struct device_attribute *attr, char *buf)
{
	return snprintf(buf, PAGE_SIZE, "Write '1' to reset\n");
}

static ssize_t ILI2511SetReset(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	char *end;
	int value = simple_strtol(buf, &end, 0);
	if(value == 0) return -EINVAL;
	gpio_set_value(EXT_TOUCH_RST, 0);
	msleep(10);
	gpio_set_value(EXT_TOUCH_RST, 1);
	msleep(300); // wait for ILI2511 to auto calibrate
	return count;
}
static int ILI2511_read_data(void);

static ssize_t ILI2511DummyRead(struct device *dev, struct device_attribute *attr, char *buf)
{
	ILI2511_read_data();
	return snprintf(buf, PAGE_SIZE, "Read!\n");
}

static ssize_t ILI2511DummyWrite(struct device *dev, struct device_attribute *attr, const char *buf, size_t count)
{
	return -1;
}

unsigned int G_irq;

static int ILI2511_read_data(void)
{
	u16 x,y;
	u8 buf[5*MAX_TOUCH_POINTS+1] = {0},touchID,maxID;
	int ret = -1;
	struct ILI2511_ts_data *data = i2c_get_clientdata(this_client);

	buf[0] = 0x10;

	I2C_CTPWrite(buf,1);
	ret=I2C_CTPRead(buf,31); //Start by read ID0-ID5

	if (ret < 0) {
		printk(KERN_ALERT "%s read_data i2c_rxdata failed: %d\n", __func__, ret);
		enable_irq(G_irq);
		return ret;
	}

	if (buf[0]&1) {
		maxID=6;
	}
	else {
		maxID=MAX_TOUCH_POINTS;
		I2C_CTPRead(buf+31,20);	//read the remainder ID6-ID9 for clearing on IRQ
	}
	
	for (touchID=0;touchID<maxID;touchID++)	{
		input_mt_slot(data->input_dev, touchID);

		if (buf[touchID*5+1] & 0x80) { 	//	Touch on ?
			data->dwork[touchID].ison = 1;
			cancel_delayed_work_sync(&data->dwork[touchID].dwork);

			input_mt_report_slot_state(data->input_dev, MT_TOOL_FINGER, true);
			input_report_abs(data->input_dev, ABS_MT_PRESSURE, 200);

			x = (((u16)buf[touchID*5+1] & 0x3F)<<8) | (u16)buf[touchID*5+2];
			y = (((u16)buf[touchID*5+3] & 0x3F)<<8) | (u16)buf[touchID*5+4];

			//printk(KERN_ALERT "%d %d\n", buf[touchID*5+1] & 0x3F, buf[touchID*5+2]);

			input_report_abs(data->input_dev, ABS_MT_POSITION_X, x);
			input_report_abs(data->input_dev, ABS_MT_POSITION_Y, y);
		}
		else if (data->dwork[touchID].ison) {
			queue_delayed_work(data->ts_workqueue, &data->dwork[touchID].dwork, msecs_to_jiffies(50));
		}
	}
	input_mt_report_pointer_emulation(data->input_dev, true);
	input_sync(data->input_dev);
        enable_irq(G_irq);

        return 0;
}


static int ILI2511_read_conf(void)
{
	u8 buf[9] = {0};
	int ret = -1;

	buf[0] = 0x20;

	I2C_CTPWrite(buf, 1);
	ret=I2C_CTPRead(buf,9);

	if (ret < 0) {
		printk(KERN_ALERT "%s read_conf i2c_rxdata failed: %d\n", __func__, ret);
		return ret;
	}
	pdata->MaxX = le16_to_cpup( (__le16*)&buf[0] );
	pdata->MaxY = le16_to_cpup( (__le16*)&buf[2] );
	//printk(KERN_ALERT "maxX: %d maxY: %d\n", pdata->MaxX, pdata->MaxY);

	return 0;
}

static void ILI2511_ts_pen_irq_work(struct work_struct *work)
{
	int ret;
	ret = ILI2511_read_data();
}

static irqreturn_t ILI2511_ts_interrupt(int irq, void *dev_id)
{
	struct ILI2511_ts_data *ILI2511_ts = dev_id;

	if (!work_pending(&ILI2511_ts->pen_event_work))
	{
		G_irq = irq;
                disable_irq_nosync(G_irq);
                queue_work(ILI2511_ts->ts_workqueue, &ILI2511_ts->pen_event_work);
	}

	return IRQ_HANDLED;
}

static void ILI2511_release_button(struct work_struct *work)
{
	struct release_work* rwork = container_of(work, struct release_work, dwork.work);
	struct ILI2511_ts_data *data = container_of(rwork, struct ILI2511_ts_data, dwork[rwork->index]);

	//printk( KERN_ALERT "release %d\n", rwork->index);
	rwork->ison = 0;
	input_mt_slot(data->input_dev, rwork->index);
	input_mt_report_slot_state(data->input_dev, MT_TOOL_FINGER, false);
	input_report_abs(data->input_dev, ABS_MT_PRESSURE, 0);
	input_mt_report_pointer_emulation(data->input_dev, true);
	input_sync(data->input_dev);
}

static int EdtILI2511Probe(struct i2c_client *client, const struct i2c_device_id *id)
{
	struct ILI2511_ts_data *ILI2511_ts;
	struct input_dev *input_dev;
	int err = 0;
	int i = 0;

	// check that the I2C adaptor is available
	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C)) {
		err = -ENODEV;
		goto exit_check_functionality_failed;
	}
	if (gpio_is_valid(EXT_TOUCH_RST)) {
		// pulls reset down, enabling the low active reset 
		err = gpio_request_one(EXT_TOUCH_RST, GPIOF_OUT_INIT_HIGH,
				       "edt-ft5x06 reset");
		if (err) {
			dev_err(&client->dev,
				"Failed to request GPIO %d as reset pin, error %d\n",
				EXT_TOUCH_RST, err);
			goto exit_check_functionality_failed;
		}
                gpio_direction_output(EXT_TOUCH_RST,0);

		msleep(10);
		gpio_set_value(EXT_TOUCH_RST, 1);
		msleep(300); // wait for ILI2511 to auto calibrate
	}
        else printk(KERN_ALERT "RST pin not valid!\n");

	// request memory for data
	ILI2511_ts = kzalloc(sizeof(*ILI2511_ts), GFP_KERNEL);
	if (!ILI2511_ts) {
		err = -ENOMEM;
		goto exit_alloc_data_failed;
	}

	// remember client pointer and assign new data to client
	this_client = client;
	i2c_set_clientdata(client, ILI2511_ts);

	//set workque function and create workque
	INIT_WORK(&ILI2511_ts->pen_event_work, ILI2511_ts_pen_irq_work);
	ILI2511_ts->ts_workqueue = create_singlethread_workqueue(dev_name(&client->dev));
	if (!ILI2511_ts->ts_workqueue) {
		err = -ESRCH;
		goto exit_create_singlethread;
	}


	for (i = 0; i < MAX_TOUCH_POINTS; i++) {
		ILI2511_ts->dwork[i].ison = 0;
		ILI2511_ts->dwork[i].index = i;
		INIT_DELAYED_WORK(&ILI2511_ts->dwork[i].dwork, ILI2511_release_button);
	}

	//check that the platform data is available
	pdata = client->dev.platform_data;
	if (pdata == NULL) {
		dev_err(&client->dev, "%s: platform data is null\n", __func__);
		goto exit_platform_data_null;
	}

	//read relevant registers and set sysfs variables
	// assign interrupt
	err = request_irq(client->irq, ILI2511_ts_interrupt, /*IRQF_DISABLED |*/ IRQF_TRIGGER_LOW, "ILI2511_ts", ILI2511_ts);
	if (err < 0) {
		dev_err(&client->dev, "ILI2511_probe: request irq failed irq=0x%x\n",client->irq);
		goto exit_irq_request_failed;
	}

	// initialize sysfs
	err = sysfs_create_group(&client->dev.kobj, &ILI2511ts_attr_group);
	if (err < 0){
		printk(KERN_ALERT "ERROR__ %u\n",__LINE__);
	}

	// get input number and event number
	input_dev = input_allocate_device();
	if (!input_dev) {
		err = -ENOMEM;
		dev_err(&client->dev, "failed to allocate input device\n");
		goto exit_input_dev_alloc_failed;
	}

	err = ILI2511_read_conf();
        if (err < 0) {
		dev_err(&client->dev, "failed to read i2c\n");
		goto exit_input_register_device_failed;
        }
	
	ILI2511_ts->input_dev = input_dev;
	input_dev->name		= CTP_NAME;

	//register single touch
	input_set_abs_params(input_dev, ABS_X, 0, pdata->MaxX, 0, 0);
	input_set_abs_params(input_dev, ABS_Y, 0, pdata->MaxY, 0, 0);
	
	//setup input device to multitouch type B
	set_bit(ABS_MT_POSITION_X, input_dev->absbit);
	set_bit(ABS_MT_POSITION_Y, input_dev->absbit);
	set_bit(ABS_MT_PRESSURE, input_dev->absbit);
	
	input_set_abs_params(input_dev, ABS_MT_POSITION_X, 0, pdata->MaxX, 0, 0);
	input_set_abs_params(input_dev, ABS_MT_POSITION_Y, 0, pdata->MaxY, 0, 0);
	input_set_abs_params(input_dev, ABS_MT_PRESSURE, 0, 255, 0, 0);
	set_bit(EV_ABS, input_dev->evbit);
	set_bit(EV_KEY, input_dev->evbit);
	set_bit(EV_SYN, input_dev->evbit);
	set_bit(BTN_TOUCH, input_dev->keybit);
	
	input_mt_init_slots(input_dev, MAX_TOUCH_POINTS, 0);
	
	// final registration of device, device is now usable
	err = input_register_device(input_dev);
	if (err) {
		dev_err(&client->dev,
			"ILI2511_ts_probe: failed to register input device: %s\n",
			dev_name(&client->dev));
		goto exit_input_register_device_failed;
	}

	printk(KERN_ALERT "%s active. (%s)\n", CTP_NAME, VERSION);
	return 0;

exit_input_register_device_failed:
	input_free_device(input_dev);
exit_input_dev_alloc_failed:
	free_irq(client->irq, ILI2511_ts);
exit_irq_request_failed:
exit_platform_data_null:
	cancel_work_sync(&ILI2511_ts->pen_event_work);
	destroy_workqueue(ILI2511_ts->ts_workqueue);
exit_create_singlethread:
	i2c_set_clientdata(client, NULL);
	kfree(ILI2511_ts);
exit_alloc_data_failed:
	gpio_free(EXT_TOUCH_RST);
exit_check_functionality_failed:
	printk(KERN_ALERT "%s FAILED: %d\n", CTP_NAME, err);
	return err;
}

static int EdtILI2511Remove(struct i2c_client *client)
{
	struct ILI2511_ts_data *ILI2511_ts = i2c_get_clientdata(client);
	int i = 0;
	gpio_free(EXT_TOUCH_RST);

	// free system IRQ function and workque
	free_irq(client->irq, ILI2511_ts);
	cancel_work_sync(&ILI2511_ts->pen_event_work);
	destroy_workqueue(ILI2511_ts->ts_workqueue);

	for (i = 0; i < MAX_TOUCH_POINTS; i++) {
		cancel_delayed_work_sync(&ILI2511_ts->dwork[i].dwork);
	}

	// remove input device
	input_unregister_device(ILI2511_ts->input_dev);

	kfree(ILI2511_ts);
	// remove driver data link from linux
	i2c_set_clientdata(client, NULL);

	printk(KERN_ALERT "%s removed\n", CTP_NAME);
	
	return 0;
}

static const struct i2c_device_id ILI2511DeviceID[] = {
	{ CTP_NAME, 0 },{ }
};

static struct i2c_driver EdtILI2511Driver = {
	.class      = I2C_CLASS_HWMON,
	.probe		= EdtILI2511Probe,
	.remove		= EdtILI2511Remove,
	.id_table	= ILI2511DeviceID,
	.driver	= {
		.name	= CTP_NAME,
		.owner	= THIS_MODULE,
	},
};
static struct ILI2511ts_platform_data ILI2511ts_platform_data = {
	.MaxX = 0xffff,
	.MaxY = 0xffff,
};

static int __init EdtILI2511Init(void)
{
	int status;
	struct i2c_adapter *adapter;
	// generates the I2C_board_info based on defines in the header file
	struct i2c_board_info info = {
		.type = CTP_NAME,
		.addr = I2C_ADR,
		.platform_data = &ILI2511ts_platform_data,
		.irq = gpio_to_irq(EXT_TOUCH_IRQ),
	};
	adapter = i2c_get_adapter(I2C_BUS_NUM);
	if (!adapter) {
		printk(KERN_ALERT "Getting adaptor Failed\n");
		return -EINVAL;
	}

	// add the device info to the I2C bus
	this_client = i2c_new_device(adapter, &info);
	if (!this_client) {
		printk(KERN_ALERT "client Failed\n");
		return -EINVAL;
        }

	// add driver to the I2C bus
	status = i2c_add_driver(&EdtILI2511Driver);

	return status;
}

static void __exit EdtILI2511Exit(void)
{
	i2c_del_driver(&EdtILI2511Driver); // remove driver from I2C bus. I2C bus will call EdtILI2511Remove()
	i2c_unregister_device(this_client); // remove the i2c_board_info from the kernel
}

module_init(EdtILI2511Init);
module_exit(EdtILI2511Exit);

MODULE_DEVICE_TABLE(i2c, ILI2511DeviceID);

MODULE_AUTHOR("<jakobl@edt-europe.com>");
MODULE_DESCRIPTION("EDT-Europe ILI2511 TouchScreen driver, ME mod");
MODULE_LICENSE("GPL");
