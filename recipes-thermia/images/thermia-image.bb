require recipes-core/images/core-image-minimal.bb

#inherit populate_sdk_qt6
RDEPENDS += "kernel-modules"

DESCRIPTION = "A small image just capable of allowing a device to boot and \
is suitable for development work."

# mtd-utils util-linux i2c-tools u-boot-fw-utils usbutils libdrm-tests   qt5everywheredemo   psplash bacnet-stack cinematicexperience qtscript  kernel-module-ili2511 qtbase-conf qtbase-plugins
#IMAGE_INSTALL:append = " mosquitto ti-sgx-ddk-um ti-sgx-ddk-km iptables ufw dropbear libdrm qtbase qtsvg qtserialport qtserialbus ldd sqlite3 avahi-daemon mdns"
IMAGE_INSTALL:append = " busybox mosquitto  dropbear libdrm  ldd sqlite3 avahi-daemon mdns usbutils ti-sgx-ddk-um ti-sgx-ddk-km psplash"
IMAGE_INSTALL:append = " qtbase qtsvg qtserialport qtserialbus"
IMAGE_FEATURE += "ssh-server-dropbear "


#IMAGE_INSTALL:append = " gdb gdbserver"
#psplash
#BUILDSDK_CXXFLAGS = "-Os -static"
#TARGET_CXXFLAGS =  "-Os -static"

#BUILDSDK_CXXFLAGS = "-Os -static"
#TARGET_CXXFLAGS =  "-Os -static"

#TOOLCHAIN_TARGET_TASK:append = " glibc-staticdev"

#DISABLE_STATIC = ""

#SDKIMAGE_FEATURES += "dev-pkgs dbg-pkgs staticdev-pkgs"
#SDKIMAGE_FEATURES += "staticdev-pkgs"

#inherit extrausers
#EXTRA_USERS_PARAMS = "usermod -P thermiatest root;"



