DESCRIPTON = "Startup scripts"
LICENSE = "MIT"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=b97a012949927931feb7793eee5ed924 "

# Recipe revision - don't forget to 'bump' when a new revision is created !
PR = "r0"
inherit allarch
# Runtime dependencies
#
# Add a line similar to the following to ensure any packages needed for the scripts to run are installed in the image.
#
# RDEPENDS_${PN} = "parted"

# SRC_URI specifies the files that are to be used as the scripts.
#
# Any valid src_uri format can be used - this example assumes the
# scripts are stored in the 'files' directory below the one in
# which this file is located.
#

#SRC_URI = "              \
#   file://hello;md5=3d3bb0ae1af93b239aefaced41602e58  \
#   file://world;md5=c3bdfa477b01f70e94c03050ab82c973    \
#   file://support-script;md5=12828df4906ce2259c61a73cd73478c1 \
#   "

SRC_URI = "              \
	file://check_update.sh  \
	file://clearfb  \
	file://lcdctrl  \
	file://load_fw.sh  \
	file://ota_update.sh  \
	file://pmicctrl  \
	file://savetime.sh  \
	file://setnet.sh  \
	file://start-app.sh  \
	file://thermia-master.sh  \
   	file://usb_status.sh  \
	file://uboot-check-update  \
   	"

#LIC_FILES_CHKSUM = "file://hello;md5=3d3bb0ae1af93b239aefaced41602e58 \
#   file://world;md5=c3bdfa477b01f70e94c03050ab82c973 \
#   file://support-script;md5=12828df4906ce2259c61a73cd73478c1 \
#"

FILES_${PN} += "/apps/*"

# This function is responsible for:
#  1) Ensuring the required directories exist in the image;
#  2) Installing the scripts in to the image;
#  3) Creating the desired runlevel links to the scripts.
#
do_install() {
    #
    # Create directories:
    #   ${D}${sysconfdir}/init.d - will hold the scripts
    #   ${D}${sysconfdir}/rcS.d  - will contain a link to the script that runs at startup
    #   ${D}${sysconfdir}/rc5.d  - will contain a link to the script that runs at runlevel=5
    #   ${D}${sbindir}           - scripts called by the above
    #
    # ${D} is effectively the root directory of the target system.
    # ${D}${sysconfdir} is where system configuration files are to be stored (e.g. /etc).
    # ${D}${sbindir} is where executable files are to be stored (e.g. /sbin).
    #
    install -d ${D}${sysconfdir}/init.d
    install -d ${D}${sysconfdir}/rcS.d
    install -d ${D}${sysconfdir}/rc1.d
    install -d ${D}${sysconfdir}/rc2.d
    install -d ${D}${sysconfdir}/rc3.d
    install -d ${D}${sysconfdir}/rc4.d
    install -d ${D}${sysconfdir}/rc5.d
    install -d ${D}${sysconfdir}/mdev
    #install -d ${D}${sbindir}
    install -d ${D}/apps


    #
    # Install files in to the image
    #
    # The files fetched via SRC_URI (above) will be in ${WORKDIR}.
    #
    install -m 0755 ${WORKDIR}/thermia-master.sh ${D}${sysconfdir}/init.d/
    install -m 0755 ${WORKDIR}/uboot-check-update ${D}${sysconfdir}/init.d/
    install -m 0755 ${WORKDIR}/clearfb ${D}${sysconfdir}/init.d/

    install -m 0755 ${WORKDIR}/ota_update.sh ${D}/apps/
    install -m 0755 ${WORKDIR}/savetime.sh ${D}/apps/
    install -m 0755 ${WORKDIR}/setnet.sh ${D}/apps/
    install -m 0755 ${WORKDIR}/usb_status.sh ${D}/apps/

    install -m 0755 ${WORKDIR}/check_update.sh ${D}${sysconfdir}/mdev/
    install -m 0755 ${WORKDIR}/load_fw.sh ${D}${sysconfdir}/mdev/

    #
    # Symbolic links can also be installed. e.g.
    #
    # ln -s support-script-link ${D}${sbindir}/support-script

    #
    # Create symbolic links from the runlevel directories to the script files.
    # Links of the form S... and K... mean the script when be called when
    # entering / exiting the runlevel designated by the containing directory.
    # For example:
    #   rc5.d/S90run-script will be called (with %1='start') when entering runlevel 5.
    #   rc5.d/K90run-script will be called (with %1='stop') when exiting runlevel 5.
    #

    ln -sf ../init.d/thermia-master.sh      	${D}${sysconfdir}/rc5.d/S90thermia-master.sh
    ln -sf ../init.d/uboot-check-update      	${D}${sysconfdir}/rc5.d/S89uboot-check-update
    ln -sf ../init.d/clearfb      		${D}${sysconfdir}/rc5.d/S50clearfb
}
