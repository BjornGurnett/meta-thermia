#!/bin/sh

DST="/media/upgrade/"

if ! [ -d $DST ];then
    echo "usb not mounted?"
    exit 1
fi

if [ -f ${DST}/.not_mounted ];then
    echo "usb not mounted?"
    exit 1
fi

exit 0