#!/bin/sh

start_app()
{
    ./thermia-gui.sh stop
    #../../../thermia-gui/scripts/apps/thermia-gui.sh stop  
    ./thermia-master.sh restart #start acts like a restart atm..
    ./thermia-gui.sh start
    #../../../thermia-gui/scripts/apps/thermia-gui.sh start
}

stop_app()
{
    #../thermia-gui/scripts/apps/thermia-gui.sh stop
    ./thermia-gui.sh stop
    ./thermia-master.sh stop
}

case "$1" in
    start)
    	start_app
    	;;
    stop)
    	stop_app
    	;;
    restart)
	    start_app
    	;;
    *)
	start_app
       	;;
esac
