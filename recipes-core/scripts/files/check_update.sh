#!/bin/sh

# update script, scan directory for files to upgrade from
#FOR NOW: uses first file found
MARKER="/tmp/update.run"
UPDATE_FILE_LIST="/tmp/update.scan"
OTHER_FILE_LIST="/tmp/import.scan"
DONE_SCAN="/tmp/done.scan"

UPDATE_SUFFIX="m2f"
FILE_ID="m2f"
FILE_ID_STOPPED="m2fstopped"
FILE_ID_AUTORUN="m2fauto"
MNT=/media/upgrade

name="`basename "$DEVNAME"`"

update_log()
{
    if [ $# == "0" ];then
        echo "UPDATE_LOG_START" `date`
        logger "`basename $0`:UPDATE_LOG_START"
        return
    fi
    logger "`basename $0`:$1"
    echo $1
}

fail()
{
    update_log "UPDATE FAILED: $1"
    rm ${MARKER}
    exit 1
}

check_dir_update()
{
OIFS="$IFS"
IFS=$'\n'
    DIR=$1
    update_log "update scanning at ${DIR}"
    if [ -f ${UPDATE_FILE_LIST} ];then
        update_log "removing old filelist: ${UPDATE_FILE_LIST}"
        rm ${UPDATE_FILE_LIST}
    fi
    
    FILES=`ls ${DIR}/*.m2f ${DIR}/*.m3f`
    for file in ${FILES};do
         update_log "scanning ${file}"
         id=`unzip -z ${file} | sed -nr 's/.*id:([-a-zA-Z0-9 \.]*).*/\1/p'`
         ver=`unzip -z ${file} | sed -nr 's/.*ver:([-a-zA-Z0-9 \.]*).*/\1/p'`
         info=`unzip -z ${file} | sed -nr 's/.*info:([-a-zA-Z0-9 \.]*).*/\1/p'`
         date=`unzip -z ${file} | sed -nr 's/.*date:([-a-zA-Z0-9 :]*).*/\1/p'`
         update_log "ID: $id"
         update_log "VER: $ver"
         update_log "INFO: $info"
         update_log "DATE: $date"

         if [ "${id}" == ${FILE_ID} ] || [ "${id}" == ${FILE_ID_STOPPED} ];then
             update_log "found file: $file" # id: $id ver: $ver"
             echo "$file|id:$id|ver:$ver|info:$info|date:$date" >> ${UPDATE_FILE_LIST}
         elif [ "${id}" == ${FILE_ID_AUTORUN} ];then
             update_log "autorun found: "$file
             AUTORUN="${AUTORUN} $file"
         else
             update_log "file skipped"
         fi
    done
    
    update_log "autorun list: $AUTORUN"
    if ! [ "${AUTORUN}" == "" ];then
        update_log "running autoscripts, skipping .scan"
        rm ${UPDATE_FILE_LIST}

        for auto in ${AUTORUN};do
            update_log "auto: "$auto
            run_update $auto
        done

        update_log "autorun done, unmounting"
        umount -l $path
        #rmdir $path
	rm ${MARKER}
        exit 0
    fi
IFS="$OIFS"
}

mount_media()
{
    update_log $name
    if [ -e /dev/${name}1 ];then
	update_log "partition exist use it instead"
	name="${name}1"
    fi
    
    path=${MNT}
    dev="/dev/$name"
    mkdir -p $path || fail "mount dir creation failed"
    update_log "mounting $dev on $path"
    mount $dev $path || fail "mount failed"
}

get_files()
{
    update_log "check_dir_update started at $path"
    check_dir_update $path

    #list all other files in import.scan
    touch ${OTHER_FILE_LIST}
    ls -1 ${path}/* | grep -v "\.m2f" > ${OTHER_FILE_LIST}
    echo 1 > ${DONE_SCAN}
}

update_log "$0 start, args: $@ [UDEV:${ACTION}]"

if [ $1 == "rescan" ];then
    if ! [ -e ${DONE_SCAN} ];then
	update_log "rescan: no upgrade found, abort"
	exit 1
    fi
    path=$MNT
    update_log "rescan files"
    get_files
    exit 0
fi

if [ -e "${MARKER}" ];then
    update_log "$0 already running, exit"
    exit 0
fi

if [ $1 == "wait" ];then
    update_log "restart in background"
    $0&
    exit 0
fi

touch ${MARKER}

update_log "sleep 2 to let usb settle"
sleep 2

#clear log?

if [ "${ACTION}" == "remove" ];then
    update_log "dev: ${DEVNAME} action:${ACTION}"
    update_log "media removed, removing ${UPDATE_FILE_LIST}"
    rm ${UPDATE_FILE_LIST}
    rm ${OTHER_FILE_LIST}
    rm ${DONE_SCAN}

    update_log "removed, unmounting"
    umount -l ${MNT}
    #rmdir ${MNT}
    rm ${MARKER}
    exit 0
fi

#check if tmp is rw: otherwise dont do anything: cant update at this point
#this can happen if starting system with usb inserted.
touch /tmp/.rw
if ! [ "$?" == "0" ];then
    update_log "tmp not rw, mount and exit check_update.sh"
    mount_media
    rm ${MARKER}
    exit 0
fi

if [ -e ${DONE_SCAN} ];then
    update_log "already found upgrade, abort"
    rm ${MARKER}
    exit 0
fi

if [ $# == "0" ];then
    update_log "mounting media"
    if [ -z "${DEVNAME}" ];then
	update_log "no device specified: exit"
	rm ${MARKER}
	exit 1
    fi
    if [ -z "${ACTION}" ];then
	update_log "no action specified: exit"
	rm ${MARKER}
	exit 1
    fi
    update_log "dev: ${DEVNAME} action:${ACTION}"
    mount_media
else
    update_log "scan mode, no device mounted..."
    #no mount here
    path=$1
fi

get_files

rm ${MARKER}
exit 0
