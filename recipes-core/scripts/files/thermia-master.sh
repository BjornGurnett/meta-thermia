#!/bin/sh

path="/apps/"
prog="thermia-master"
prog_args="-qws"

log()
{
    echo "$1"
    logger "`basename $0`:$1"
}

APP_ARGS=""
get_ubootparams()
{
    APP_ARGS=`fw_printenv -n appargs` 2>/dev/null
    if [ $? == "1" ];then
	APP_ARGS=""
    fi
    if [ -n "${APP_ARGS}" ];then
	log "start app with apecial args: $APP_ARGS"
    fi
}

pid=""
get_pid()
{
	pid="`pidof ${prog}`"
}

start_app()
{
    get_ubootparams
    if [ "${APP_ARGS}" == "appdisabled" ];then
	log "not starting ${prog}; check appargs"
    else
	stop_app
	
	log "starting ${prog}..."
	cd $path

	export QWS_MOUSE_PROTO='tslib:/dev/input/touchscreen'
    	./lcdctrl full
	./${prog} ${prog_args} ${APP_ARGS} ${@} &
    fi
}

stop_app()
{
    log "stopping ${prog}"
    get_pid
    
    if [ -n "${pid}" ];then
        if ! [ -e /tmp/masterInput ];then
            echo "no fifo available, wont continue"
            exit 1
        fi
        echo quit > /tmp/masterInput
    fi

    while [ -n "${pid}" ]; do #-n: (returns true when non-zero)
        sleep 1
        get_pid
    done
}

check_status()
{
    log "check for reboot/restart"

    if [ -f /tmp/factory.reset ];then
	log "app want factory reset (accepts --hard)"
	cd /apps/
	./fact_reset.sh `cat /tmp/factory.reset`
	rm /tmp/factory.reset
    fi

    if [ -f /tmp/update.reboot ];then
	rm /tmp/update.reboot
	log "update want reboot..."
	reboot
    elif [ -f /tmp/update.restart ];then
	log "update want restart..."
	rm /tmp/update.restart
	log "restarting can..."
	/etc/init.d/can restart
	cd /apps/
	./start-app.sh&
    elif [ -f /tmp/update.install ];then
	file=`cat /tmp/update.install`
	pass=`cat /tmp/update.pass`
	rm /tmp/update.install
	rm /tmp/update.pass
	log "app want install while stopped: $file"
	run_update $file $pass
	echo $? > /tmp/update.status
    elif [ -f /tmp/update.halt ];then
	rm /tmp/update.halt
	echo "shutdown system..."
	./pmicctrl -l 0
	echo "poweroff SYS"
	echo 1 > /sys/devices/virtual/gpio/gpio19/value
	halt
    fi
}


case "$1" in
    start)
    	start_app
    	;;
    stop)
    	stop_app
    	;;
    restart)
    	$0 stop
   	    $0 start
    	;;
    *)
        echo "Usage: $0 {start|stop|restart}" >&2
        exit 1
       	;;
esac
