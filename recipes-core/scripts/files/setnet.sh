#!/bin/sh
show_help() {
cat << EOF
Usage: ${0##*/} [-h] [-d netdevice] [-i ip-addr] [-m mask] [-g default-gateway]
                 [-r dns-resolver] [-t dns-resolver2] [-c] [-s]

    -h          display this help and exit
    -d          netdevice (default eth0)
EOF
}

setDhcp() {
    killall udhcpc
    udhcpc -i eth0 -f -p /var/run/udhcpc.eth0.pid
}

setStatic() {
    if [ -f /var/run/udhcpc.eth0.pid ]; then
        kill `cat /var/run/udhcpc.eth0.pid`
        rm /var/run/udhcpc.eth0.pid
    else 
        killall udhcpc
    fi
}

OPTIND=1

netdev="eth0"
ipaddr=""
ipmaskarg=""
defgw=""
resolver=""
resolver2=""
do_ifconfig=""
do_route=""

while getopts "hd:i:m:g:r:t:cs" opt; do
    case "$opt" in
    h)
        show_help
        exit 0
        ;;
    d)
        netdev=$OPTARG
        ;;
    i)
        ipaddr=$OPTARG
        do_ifconfig="y"
        ;;
    m)
        ipmaskarg="netmask $OPTARG"
        do_ifconfig="y"
        ;;
    g)
        defgw=$OPTARG
        do_route="y"
        ;;
    r)
        resolver=$OPTARG
        do_dns="y"
        ;;
    t)
        resolver2=$OPTARG
        do_doubleDns="y"
        ;;
    c)
        setDhcp
        exit 0
        ;;
    s)
        setStatic
        exit 0
        ;;
    esac
done
shift "$((OPTIND-1))"

# echo netdev=$netdev  ip=$ipaddr  mask=$ipmaskarg  gw=$defgw

if [ "$do_ifconfig" ]; then
    ifconfig $netdev $ipaddr $ipmaskarg
fi

if [ "$do_route" ]; then
    route del default >/dev/null 2>&1
    route add default gw $defgw $netdev
fi

if [ "$do_dns" ]; then
    echo nameserver $resolver > /etc/resolv.conf
    if [ "$do_doubleDns" ]; then
        echo nameserver $resolver2 >> /etc/resolv.conf
    fi
fi
