#!/bin/sh

#OIFS="$IFS"
#IFS=$'\n'

UPDATE_TMP="/dev/shm/update_tmp"
#FILE="/var/volatile/genesis.m2f"
FILE=$1
PASSWORD=$2

echo "starting script"

echo "creating update directory"
rm -fR ${UPDATE_TMP}
mkdir -p ${UPDATE_TMP}

echo "unziping update file"
if [ ! -f ${FILE} ] 
then 
	exit 1
fi
echo ${FILE}
echo ${PASSWORD}
echo ${1}
echo ${2}
unzip ${PASSWORD} ${FILE} -d ${UPDATE_TMP} 
if [[ $? == 0 ]] ; then
echo "Success"
else
echo "unzip failed"
exit 1
fi ;

cd ${UPDATE_TMP}

cd ${UPDATE_TMP}
echo "running update script"
if [ ! -f "update.sh" ] 
then 
exit 1
fi

sh ./update.sh
reboot
echo "script finished"
#IFS="$OIFS"
exit 0
