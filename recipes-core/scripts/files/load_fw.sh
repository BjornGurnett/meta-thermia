#!/bin/sh

fail()
{
    echo "FAILED, FIXME:  need reboot" > /dev/console
    echo "FIXME: mark file not found to kernel" > /dev/console
    exit 1
}

if ! [ "$FIRMWARE" = "" ];then
    echo "load firmware: $FIRMWARE" > /dev/console
    echo 1 > /sys$DEVPATH/loading
    cat "/lib/firmware/$FIRMWARE" > /sys$DEVPATH/data || fail
    echo 0 > /sys$DEVPATH/loading
    echo "OK" > /dev/console
fi