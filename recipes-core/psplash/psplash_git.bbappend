FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SRC_URI:append=" file://psplash-thermia-img-img.h \
		file://psplash-ochsner-img-img.h"

SPLASH_IMAGES = "file://psplash-thermia-img-img.h;outsuffix=default \
		 file://psplash-thermia-img-img.h;outsuffix=steibel \
		 file://psplash-ochsner-img-img.h;outsuffix=ochsner" 
RRECOMMENDS_${PN} = "psplash-ochsner psplash-steibel"

